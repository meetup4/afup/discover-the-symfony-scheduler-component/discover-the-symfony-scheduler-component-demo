<?php

$finder = PhpCsFixer\Finder::create()
    ->in(__DIR__)
    ->exclude('var')
    ->exclude('vendor')
    ->exclude('config')
    ->notPath('src/Kernel.php')
    ->notPath('public/index.php')
;

$config = new PhpCsFixer\Config();
return $config->setRules([
    '@Symfony' => true,
    'declare_strict_types' => true,
    'yoda_style' => false,
    'array_syntax' => ['syntax' => 'short'],
])
    ->setRiskyAllowed(true)
    ->setUsingCache(false)
    ->setFinder($finder)
;
