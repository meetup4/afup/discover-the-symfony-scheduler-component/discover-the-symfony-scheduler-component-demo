FROM composer

FROM php:8.3-fpm-alpine AS php_base

RUN apk upgrade && \
    apk update && \
    apk add --no-cache acl bash git make shadow tzdata libfaketime alsa-utils pulseaudio pulseaudio-alsa mplayer

COPY --from=mlocati/php-extension-installer /usr/bin/install-php-extensions /usr/local/bin/
RUN install-php-extensions apcu intl opcache zip gd

###> recipes ###
###< recipes ###

FROM php_base AS php_build

ARG APP_UID=1000
ARG APP_GID=1000
ARG AUDIO_GID=18

RUN usermod --uid ${APP_UID} www-data && \
    groupmod --gid ${APP_GID} www-data && \
    groupmod --gid ${AUDIO_GID} audio && \
    addgroup www-data audio

COPY --chown=www-data:www-data --from=composer /usr/bin/composer /usr/bin/composer

USER www-data

FROM php_build AS php_dev

USER root

RUN apk add --no-cache --virtual .build-deps $PHPIZE_DEPS autoconf && \
    apk del .build-deps

COPY --link docker/php/conf.d/php.ini $PHP_INI_DIR/php.ini
COPY --link docker/php/conf.d/symfony.ini $PHP_INI_DIR/conf.d/

USER www-data
