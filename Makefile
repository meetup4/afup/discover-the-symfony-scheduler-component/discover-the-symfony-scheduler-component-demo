DOCKER_COMPOSE = docker compose

APP_UID = $(shell id -u)
APP_GID = $(shell id -g)
AUDIO_GID = $(shell getent group audio | cut -d: -f3)

EXEC_PHP = $(DOCKER_COMPOSE) exec -T php
COMPOSER = $(EXEC_PHP) composer
CONSOLE	= $(EXEC_PHP) bin/console

RUN_QA = $(DOCKER_COMPOSE) run --rm qa
PHP_CS_FIXER = $(RUN_QA) php-cs-fixer

##     _____                  __                     _____      _              _       _                 _
##    / ____|                / _|                   / ____|    | |            | |     | |               | |
##   | (___  _   _ _ __ ___ | |_ ___  _ __  _   _  | (___   ___| |__   ___  __| |_   _| | ___ _ __    __| | ___ _ __ ___   ___
##    \___ \| | | | '_ ` _ \|  _/ _ \| '_ \| | | |  \___ \ / __| '_ \ / _ \/ _` | | | | |/ _ \ '__|  / _` |/ _ \ '_ ` _ \ / _ \
##    ____) | |_| | | | | | | || (_) | | | | |_| |  ____) | (__| | | |  __/ (_| | |_| | |  __/ |    | (_| |  __/ | | | | | (_) |
##   |_____/ \__, |_| |_| |_|_| \___/|_| |_|\__, | |_____/ \___|_| |_|\___|\__,_|\__,_|_|\___|_|     \__,_|\___|_| |_| |_|\___/
##            __/ |                          __/ |
##           |___/                          |___/

##
##Setup 🐳
##--------

install: ## Install the project
install: build start vendor
.PHONY: install

clean: ## Remove Docker containers and delete generated files/folders
clean: kill
	rm -rf var vendor
.PHONY: clean

restart: ## Restart Docker containers
restart: stop start
.PHONY: restart

reset: ## Clean and install the project
reset: clean install
.PHONY: reset

build:
	$(DOCKER_COMPOSE) build --pull --build-arg "APP_UID=${APP_UID}" --build-arg "APP_GID=${APP_GID}" --build-arg "AUDIO_GID=${AUDIO_GID}"
.PHONY: build

start:
	$(DOCKER_COMPOSE) up -d --remove-orphans $(services)
.PHONY: start

stop:
	$(DOCKER_COMPOSE) stop $(services)
.PHONY: stop

kill:
	$(DOCKER_COMPOSE) kill
	$(DOCKER_COMPOSE) down --volumes --remove-orphans
.PHONY: kill

##
##Symfony 🚀
##----------

sf: ## List Symfony commands. You can also run Symfony commands (e.g. make sf command='about')
	$(CONSOLE) $(or $(command), list)
.PHONY: sf

clock: ## Launches the clock command
	$(CONSOLE) app:clock
.PHONY: clock

debug-scheduler: ## List schedule
	$(CONSOLE) debug:scheduler
.PHONY: debug-scheduler

consume: ## Consume schedule transport (e.g. make consume transport='scheduler_clock')
	$(CONSOLE) messenger:consume $(or $(transport), scheduler_default)
.PHONY: consume

##
##Composer 🛠️
##-----------

composer: ## List Composer commands. You can also run Composer commands (e.g. make composer command='--version')
	$(COMPOSER) $(or $(command), list)
.PHONY: composer

vendor: composer.lock
	$(COMPOSER) install -n --prefer-dist
.PHONY: vendor

##
##QA 🪲
##-----

php-cs-fixer-dry: ## PHP-CS-Fixer (https://cs.symfony.com)
	$(PHP_CS_FIXER) fix --dry-run --verbose --diff
.PHONY: php-cs-fixer-dry

php-cs-fixer-apply: ## Apply PHP-CS-Fixer fixes
	$(PHP_CS_FIXER) fix --verbose --diff
.PHONY: php-cs-fixer-apply

##
##Shell 🐚
##--------

sh: ## Open a shell in a Docker Compose service (e.g. make sh user='www-data' service='php')
	$(DOCKER_COMPOSE) exec -u $(or $(user), root) $(or $(service), php) sh
.PHONY: sh

sh-exec: ## Exec a shell command in a Docker Compose service (e.g. make sh-exec command='id')
	$(DOCKER_COMPOSE) exec -u $(or $(user), root) $(or $(service), php) $(or $(command), whoami)
.PHONY: sh-exec

##
##Help ℹ️
##-------

help: ## List Makefile commands
	@grep -E '(^[a-zA-Z0-9_-]+:.*?##.*$$)|(^##)' $(MAKEFILE_LIST) | awk 'BEGIN {FS = ":.*?## "}; {printf "\033[32m%-30s\033[0m %s\n", $$1, $$2}' | sed -e 's/\[32m##/[33m/'
.PHONY: help

.DEFAULT_GOAL := help
