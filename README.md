# Unlock the power of time: Discover the Symfony Scheduler component!

> ⚠️ This repository was created for a Linux operating system.
>
> If you're using a different operating system, you'll need to make some changes.

## Requirements

- ✨ [Make](https://www.gnu.org/software/make/)
- 🐋 [Docker](https://www.docker.com/)
- 🐳 [Docker Compose](https://docs.docker.com/compose/)
- 🎶 [PulseAudio](https://www.freedesktop.org/wiki/Software/PulseAudio/)

## Pre-install

### ALSA

List audio device names

```shell
$ aplay -L
$ # or
$ cat /proc/asound/cards
```

> ℹ️ Change the name of your sound card in .env (ALSA_CARD)

## Install

```shell
$ make install
```

## How to use

### Clock command

```shell
$ make clock
```

### Debug schedules

```shell
$ make debug-scheduler
```

### Consume schedules

```shell
$ make consume
```

## Uninstall

```shell
$ make clean
```

## Reinstall

```shell
$ make reset
```

## List commands

```shell
$ make
$ # or
$ make help
```
