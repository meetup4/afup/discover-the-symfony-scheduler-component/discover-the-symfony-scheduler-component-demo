<?php

declare(strict_types=1);

namespace App\Command;

use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\DependencyInjection\Attribute\Autowire;
use Symfony\Component\Scheduler\Attribute\AsPeriodicTask;

#[AsCommand(name: 'app:clock')]
#[AsPeriodicTask(frequency: '1 minute', from: '2024-05-22', until: '2024-12-31', schedule: 'command')]
// #[AsCronTask('* * * * *')] (require dragonmantank/cron-expression)
final class ClockCommand extends Command
{
    public function __construct(
        #[Autowire(env: 'CLOCK_MUSIC_FOLDER')]
        private readonly string $clockMusicFolder,
        #[Autowire(env: 'CLOCK_MUSIC')]
        private readonly string $clockMusic
    ) {
        parent::__construct();
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $command = sprintf('mplayer %s/%s', $this->clockMusicFolder, $this->clockMusic);

        shell_exec($command);

        return Command::SUCCESS;
    }
}
