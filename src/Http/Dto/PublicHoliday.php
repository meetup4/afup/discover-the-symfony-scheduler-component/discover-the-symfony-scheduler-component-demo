<?php

declare(strict_types=1);

namespace App\Http\Dto;

final readonly class PublicHoliday
{
    public function __construct(
        public string $id,
        public string $name,
        public bool $nationwide,
        public \DateTimeImmutable $startDate,
        public \DateTimeImmutable $endDate,
        public string $type,
    ) {
    }
}
