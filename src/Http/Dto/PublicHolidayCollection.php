<?php

declare(strict_types=1);

namespace App\Http\Dto;

use ArrayIterator;

/**
 * @extends ArrayIterator<string, PublicHoliday>
 */
final class PublicHolidayCollection extends \ArrayIterator
{
    /**
     * @param PublicHoliday[] $publicHolidays
     */
    public function __construct(array $publicHolidays)
    {
        parent::__construct($publicHolidays);
    }

    public function isPublicHoliday(\DateTimeImmutable $date): bool
    {
        $dates = array_map(
            fn (PublicHoliday $publicHoliday): bool => $publicHoliday->startDate->format('Y-m-d') === $date->format('Y-m-d')
                || $publicHoliday->endDate->format('Y-m-d') === $date->format('Y-m-d'),
            $this->getArrayCopy()
        );

        return in_array(true, $dates, true);
    }
}
