<?php

declare(strict_types=1);

namespace App\Http;

use App\Http\Dto\PublicHoliday;
use App\Http\Dto\PublicHolidayCollection;
use Symfony\Component\Serializer\SerializerInterface;
use Symfony\Contracts\HttpClient\Exception\ClientExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\RedirectionExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\ServerExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface;
use Symfony\Contracts\HttpClient\HttpClientInterface;

final class OpenHolidaysClient implements PublicHolidaysInterface
{
    public function __construct(
        private readonly HttpClientInterface $openholidaysClient,
        private readonly SerializerInterface $serializer,
    ) {
    }

    /**
     * @throws \Exception
     * @throws \JsonException
     * @throws TransportExceptionInterface
     * @throws ServerExceptionInterface
     * @throws RedirectionExceptionInterface
     * @throws ClientExceptionInterface
     */
    public function getPublicHolidays(string $country): PublicHolidayCollection
    {
        $response = $this->openholidaysClient->request('GET', '/PublicHolidays', [
            'query' => [
                'countryIsoCode' => $country,
                'languageIsoCode' => $country,
                'validFrom' => (new \DateTimeImmutable(sprintf('%s-01-01', date('Y'))))->format('Y-m-d'),
                'validTo' => (new \DateTimeImmutable(sprintf('%s-12-31', date('Y'))))->format('Y-m-d'),
            ],
        ])->getContent();

        if (!json_validate($response)) {
            throw new \JsonException('Does not contain a valid json');
        }

        /** @var PublicHolidayCollection $publicHolidays */
        $publicHolidays = $this->serializer->deserialize($response, PublicHoliday::class, 'json');

        return $publicHolidays;
    }
}
