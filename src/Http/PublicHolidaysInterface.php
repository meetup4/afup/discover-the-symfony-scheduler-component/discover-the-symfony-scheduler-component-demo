<?php

declare(strict_types=1);

namespace App\Http;

use App\Http\Dto\PublicHolidayCollection;

interface PublicHolidaysInterface
{
    public function getPublicHolidays(string $country): PublicHolidayCollection;
}
