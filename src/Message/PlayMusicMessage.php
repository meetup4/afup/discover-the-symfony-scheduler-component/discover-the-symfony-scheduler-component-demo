<?php

declare(strict_types=1);

namespace App\Message;

use Symfony\Component\DependencyInjection\Attribute\Autowire;

final readonly class PlayMusicMessage
{
    public function __construct(
        #[Autowire(env: 'CLOCK_MUSIC')]
        public string $clockMusic
    ) {
    }
}
