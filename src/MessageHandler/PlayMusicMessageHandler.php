<?php

declare(strict_types=1);

namespace App\MessageHandler;

use App\Message\PlayMusicMessage;
use Symfony\Component\DependencyInjection\Attribute\Autowire;
use Symfony\Component\Messenger\Attribute\AsMessageHandler;

#[AsMessageHandler]
final class PlayMusicMessageHandler
{
    public function __construct(
        #[Autowire(env: 'CLOCK_MUSIC_FOLDER')]
        private readonly string $clockMusicFolder,
    ) {
    }

    public function __invoke(PlayMusicMessage $message): void
    {
        $command = sprintf('mplayer %s/%s', $this->clockMusicFolder, $message->clockMusic);

        shell_exec($command);
    }
}
