<?php

declare(strict_types=1);

namespace App\Scheduler;

use App\Message\PlayMusicMessage;
use Symfony\Component\Scheduler\Attribute\AsSchedule;
use Symfony\Component\Scheduler\RecurringMessage;
use Symfony\Component\Scheduler\Schedule;
use Symfony\Component\Scheduler\ScheduleProviderInterface;
use Symfony\Contracts\Cache\CacheInterface;

#[AsSchedule]
final class ClockSchedule implements ScheduleProviderInterface
{
    public function __construct(
        private readonly CacheInterface $cache,
        private readonly PlayMusicMessage $playMusicMessage,
    ) {
    }

    /**
     * @throws \Exception
     */
    public function getSchedule(): Schedule
    {
        $from = new \DateTimeImmutable('07:30', new \DateTimeZone('Europe/Paris'));

        return (new Schedule())
            ->add(
                RecurringMessage::every('1 day', $this->playMusicMessage, $from),
            )
            ->stateful($this->cache)
        ;
    }
}
