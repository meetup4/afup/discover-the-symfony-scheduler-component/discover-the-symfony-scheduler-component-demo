<?php

declare(strict_types=1);

namespace App\Scheduler;

use App\Http\PublicHolidaysInterface;
use App\Message\PlayMusicMessage;
use App\Scheduler\Trigger\ExcludePublicHolidaysTrigger;
use Symfony\Component\Scheduler\Attribute\AsSchedule;
use Symfony\Component\Scheduler\RecurringMessage;
use Symfony\Component\Scheduler\Schedule;
use Symfony\Component\Scheduler\ScheduleProviderInterface;
use Symfony\Component\Scheduler\Trigger\PeriodicalTrigger;
use Symfony\Contracts\Cache\CacheInterface;

#[AsSchedule('clock')]
final class ClockWithoutPublicHolidaysSchedule implements ScheduleProviderInterface
{
    public function __construct(
        private ?Schedule $schedule,
        private readonly CacheInterface $cache,
        private readonly PlayMusicMessage $playMusicMessage,
        private readonly PublicHolidaysInterface $publicHolidays,
    ) {
    }

    /**
     * @throws \Exception
     */
    public function getSchedule(): Schedule
    {
        $from = new \DateTimeImmutable('07:30', new \DateTimeZone('Europe/Paris'));

        return $this->schedule ??= (new Schedule())
            ->with(
                RecurringMessage::trigger(
                    new ExcludePublicHolidaysTrigger(
                        new PeriodicalTrigger('1 day', $from),
                        ExcludePublicHolidaysTrigger::FRANCE,
                        $this->publicHolidays
                    ),
                    $this->playMusicMessage,
                ),
            )
            ->stateful($this->cache)
        ;
    }
}
