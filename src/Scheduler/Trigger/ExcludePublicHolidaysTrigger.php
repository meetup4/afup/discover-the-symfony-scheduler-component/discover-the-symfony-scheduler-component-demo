<?php

declare(strict_types=1);

namespace App\Scheduler\Trigger;

use App\Http\PublicHolidaysInterface;
use Symfony\Component\Scheduler\Trigger\TriggerInterface;

final class ExcludePublicHolidaysTrigger implements TriggerInterface
{
    public const string FRANCE = 'FR';

    public function __construct(
        private readonly TriggerInterface $inner,
        private readonly string $country,
        private readonly PublicHolidaysInterface $publicHolidays,
    ) {
    }

    public function __toString(): string
    {
        return sprintf('%s (except public holidays)', $this->inner);
    }

    public function getNextRunDate(\DateTimeImmutable $run): ?\DateTimeImmutable
    {
        if (!$nextRun = $this->inner->getNextRunDate($run)) {
            return null;
        }

        // loop until you get the next run date that is not a public holiday
        while ($this->isPublicHoliday($nextRun)) {
            $nextRun = $this->inner->getNextRunDate($nextRun);
        }

        return $nextRun;
    }

    private function isPublicHoliday(\DateTimeImmutable $timestamp): bool
    {
        $publicHolidays = $this->publicHolidays->getPublicHolidays($this->country);

        return match ($this->country) {
            self::FRANCE => $publicHolidays->isPublicHoliday($timestamp),
            default => false,
        };
    }
}
