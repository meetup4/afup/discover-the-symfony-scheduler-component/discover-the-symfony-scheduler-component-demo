<?php

declare(strict_types=1);

namespace App\Serializer\Normalizer;

use App\Http\Dto\PublicHoliday;
use App\Http\Dto\PublicHolidayCollection;
use Symfony\Component\Serializer\Normalizer\DenormalizerInterface;

final class PublicHolidaysNormalizer implements DenormalizerInterface
{
    /**
     * @throws \Exception
     */
    public function denormalize(mixed $data, string $type, ?string $format = null, array $context = []): PublicHolidayCollection
    {
        if (is_array($data)) {
            $publicHolidays = [];

            foreach ($data as $publicHoliday) {
                $publicHolidays[] = new PublicHoliday(
                    $publicHoliday['id'],
                    current($publicHoliday['name'])['text'],
                    (bool) $publicHoliday['nationwide'],
                    new \DateTimeImmutable($publicHoliday['startDate']),
                    new \DateTimeImmutable($publicHoliday['endDate']),
                    $publicHoliday['type'],
                );
            }

            return new PublicHolidayCollection($publicHolidays);
        }

        throw new \LogicException('Cannot denormalize data');
    }

    public function supportsDenormalization(mixed $data, string $type, ?string $format = null, array $context = []): bool
    {
        return $type === PublicHoliday::class && $format === 'json';
    }

    public function getSupportedTypes(?string $format): array
    {
        return [PublicHoliday::class => true];
    }
}
